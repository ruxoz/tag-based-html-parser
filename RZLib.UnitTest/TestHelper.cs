﻿using System;
using System.Reflection;

namespace UnitTestUtilities
{
    static class TestHelper
    {
        #region Constructor
        public static Object CreateInstance(String fullType, Type[] ctorParams, Object[] parameters)
        {
            var actualType = Type.GetType(fullType, true);

            var ctor = actualType.GetConstructor(
                BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public,
                null,
                ctorParams,
                null
                );

            return ctor.Invoke(parameters);
        }
        #endregion

        #region Get Field
        public static T GetInstanceField<T>(this Object instance, String fieldName)
        {
            var field = instance.GetType().GetField(fieldName, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

            return (T)field.GetValue(instance);
        }
        #endregion

        #region Method Call
        public static T CallInstanceGetProperty<T>(this Object instance, String propertyName)
        {
            var property = instance.GetType().GetProperty(
                propertyName,
                BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic
                );

            var method = property.GetGetMethod();

            return (T)method.Invoke(instance, null);

        }
        #endregion

        #region Run Method

        /// <summary>
        ///		Runs a method on a type, given its parameters. This is useful for
        ///		calling private methods.
        /// </summary>
        /// <param name="t"></param>
        /// <param name="strMethod"></param>
        /// <param name="aobjParams"></param>
        /// <returns>The return value of the called method.</returns>
        public static object RunStaticMethod(System.Type t, string strMethod, object[] aobjParams)
        {
            BindingFlags eFlags = BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic;
            return RunMethod(t, strMethod, null, aobjParams, eFlags);
        }

        public static T RunInstanceMethod<T>(this Object objInstance, string strMethod, params object[] aobjParams)
        {
            try
            {
                return (T)RunMethod
                    (objInstance.GetType(),
                    strMethod,
                    objInstance,
                    aobjParams,
                    BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic
                    );
            }
            catch (TargetInvocationException ex)
            {
                System.Diagnostics.Debug.Assert(ex.InnerException != null);
                throw ex.InnerException;
            }
        }

        private static object RunMethod(System.Type t, string strMethod, object objInstance, object[] aobjParams, BindingFlags eFlags)
        {
            MethodInfo m;
            try
            {
                m = t.GetMethod(strMethod, eFlags);
                if (m == null)
                {
                    throw new ArgumentException("There is no method '" + strMethod + "' for type '" + t.ToString() + "'.");
                }

                object objRet = m.Invoke(objInstance, aobjParams);
                return objRet;
            }
            catch
            {
                throw;
            }
        }

        #endregion

    }

}
