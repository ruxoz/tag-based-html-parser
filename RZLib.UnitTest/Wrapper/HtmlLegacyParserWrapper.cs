﻿using System;
using UnitTestUtilities;
using RZ.Web;

namespace RZ.UnitTest.Wrapper
{
    sealed class HtmlLegacyParserWrapper
    {
        Object parser;

        public HtmlLegacyParserWrapper(HtmlLexerWrapper lexer)
        {
            Type[] paramType = { lexer.NativeLexer.GetType() };
            Object[] parameters = { lexer.NativeLexer };

            this.parser = TestHelper.CreateInstance(
                "RZ.Web.HtmlLegacyParser, RZLib",
                paramType,
                parameters
                );
        }

        public Boolean GetHtmlCompleteTag(out HtmlContent content)
        {
            Object[] parameters = { null };
            Boolean returnValue = parser.RunInstanceMethod<Boolean>("GetHtmlCompleteTag", parameters);

            content = (HtmlContent)parameters[0];

            return returnValue;
        }

        public Boolean GetHtmlCloseTag(out HtmlContent content)
        {
            Object[] parameters = { null };
            Boolean returnValue = parser.RunInstanceMethod<Boolean>("GetHtmlCloseTag", parameters);

            content = (HtmlContent)parameters[0];

            return returnValue;
        }

        public Boolean GetHtmlOpenTag(out HtmlContent content)
        {
            Object[] parameters = { null };
            Boolean returnValue = parser.RunInstanceMethod<Boolean>("GetHtmlOpenTag", parameters);

            content = (HtmlContent)parameters[0];

            return returnValue;
        }

        public Boolean GetNormalText(out HtmlContent content)
        {
            Object[] parameters = { null };
            Boolean returnValue = parser.RunInstanceMethod<Boolean>("GetNormalText", parameters);

            content = (HtmlContent)parameters[0];

            return returnValue;
        }
    }
}
