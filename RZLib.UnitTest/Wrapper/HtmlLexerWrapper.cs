﻿using System;
using UnitTestUtilities;
using System.Reflection;
using System.Diagnostics;

namespace RZ.UnitTest.Wrapper
{
    sealed class HtmlLexerWrapper
    {
        Object lexer;

        public HtmlLexerWrapper(String content)
        {
            Type[] paramType = { typeof(String) };
            Object[] parameters = { content };

            this.lexer = TestHelper.CreateInstance(
                "RZ.Web.HtmlLegacyLexer, RZLib",
                paramType,
                parameters
                );
        }

        internal HtmlLexerWrapper(Object lexer)
        {
            Debug.Assert(lexer != null);
            this.lexer = lexer;
        }

        internal Object NativeLexer
        {
            get { return this.lexer; }
        }

        public String Content
        {
            get { return this.lexer.CallInstanceGetProperty<String>("Content"); }
        }

        public Int32 Cursor
        {
            get { return this.lexer.CallInstanceGetProperty<Int32>("Cursor"); }
        }

        public Boolean EOC
        {
            get { return this.lexer.CallInstanceGetProperty<Boolean>("EOC"); }
        }

        public Boolean GetBeginOpenTag(out String tagName)
        {
            return CallLexerMethod("GetBeginOpenTag", out tagName);
        }

        public Boolean GetTagName(out String tagName)
        {
            return CallLexerMethod("GetTagName", out tagName);
        }

        public void SkipCompleteEndOpenTag()
        {
            this.lexer.RunInstanceMethod<Object>("SkipCompleteEndOpenTag");
        }

        public void SkipWhiteSpaces()
        {
            this.lexer.RunInstanceMethod<Object>("SkipWhiteSpaces");
        }

        public Boolean GetQuoteText(out String text)
        {
            return CallLexerMethod("GetQuoteText", out text);
        }

        public Boolean GetSingleCompromisedWord(out String text)
        {
            return CallLexerMethod("GetSingleCompromiseWord", out text);
        }

        public Boolean GetNormalText(out String text)
        {
            Object[] parameter = { null };
            var gettable = this.lexer.RunInstanceMethod<Boolean>("GetNormalText", parameter);

            text = (String) parameter[0];
            return gettable;
        }

        Boolean CallLexerMethod(String methodName, out String output)
        {
            Object[] parameter = { null };
            var gettable = this.lexer.RunInstanceMethod<Boolean>(methodName, parameter);
            output = (String)parameter[0];
            return gettable;
        }
    }
}
