﻿using System.Linq;
using FluentAssertions;
using RZ.UnitTest.Wrapper;
using RZ.Web;
using System;
using Xunit;

namespace RZ.UnitTest.Web {
    public sealed class HtmlLegacyParserTest {
        [Fact]
        public void Comment(){
            var parser = new HtmlLegacyParser("<!-- some comment --> normal text");

            var result = parser.ToArray();

            result.Length.Should().Be(2);
            result[0].Should().BeOfType<HtmlCommentTag>();
            result[0].ToString().Should().Be("<!-- some comment -->");

            result[1].Should().BeOfType<HtmlContentText>();
            result[1].ToString().Should().Be(" normal text");
        }

        #region GetHtmlCloseTag Tests
        [Fact]
        public void CloseTag()
        {
            var tag = FindCloseTag("</title>\n<meta />");

            tag.TagName.ToString().Should().Be("title");
        }

        [Fact]
        public void CloseTag_WithInnerSpace()
        {
            var tag = FindCloseTag("</title   >\n<meta />");

            tag.TagName.ToString().Should().Be("title");
        }

        [Fact]
        public void InvalidCloseTag()
        {
            var lexer = new HtmlLexerWrapper("</  title   >\n<meta />");
            var parser = new HtmlLegacyParserWrapper(lexer);

            HtmlContent content;
            parser.GetHtmlCloseTag(out content).Should().BeFalse();
        }

        HtmlContentCloseTag FindCloseTag(String text)
        {
            var lexer = new HtmlLexerWrapper(text);
            var parser = new HtmlLegacyParserWrapper(lexer);

            HtmlContent content;

            parser.GetHtmlCloseTag(out content).Should().BeTrue();
            content.Should().BeOfType<HtmlContentCloseTag>();

            var closeTag = (HtmlContentCloseTag)content;

            closeTag.IsClosed.Should().BeTrue();

            return closeTag;
        }
        #endregion

        #region GetHtmlOpenTag Tests
        [Fact]
        public void SimpleOpenTag()
        {
            var openTag = FindTag("<title>Hello world</title>");
            openTag.TagName.ToString().Should().Be("title");
            openTag.Attributes.Should().BeEmpty();
        }

        [Fact]
        public void SimpleOpenTag_WithFrontSpace()
        {
            var openTag = FindTag("  \t \n  <title>Hello world</title>");
            openTag.TagName.ToString().Should().Be("title");
            openTag.Attributes.Should().BeEmpty();
        }

        [Fact]
        public void OpenTag_WithAttributes()
        {
            var openTag = FindTag("  <span style=\"color: red\" id='label' >Red Text</span>");

            openTag.TagName.ToString().Should().Be("span");
            openTag.Attributes.Should().NotBeEmpty();
            openTag.Attributes.Count.Should().Be(2);

            openTag.Attributes["style"].Should().Be("color: red");
            openTag.Attributes["id"].Should().Be("label");
        }

        HtmlContentOpenTag FindTag(String text)
        {
            var lexer = new HtmlLexerWrapper(text);
            var parser = new HtmlLegacyParserWrapper(lexer);

            HtmlContent content;

            parser.GetHtmlOpenTag(out content);

            return (HtmlContentOpenTag)content;
        }
        #endregion

        #region GetNormalText Tests
        [Fact]
        public void GetNormalText()
        {
            var lexer = new HtmlLexerWrapper("Hello, <b>world</b>!");
            var parser = new HtmlLegacyParserWrapper(lexer);

            HtmlContent content;
            parser.GetNormalText(out content);

            var text = (HtmlContentText)content;

            text.ToString().Should().Be("Hello, ");
        }
        #endregion
    }
}
