﻿using FluentAssertions;
using RZ.Web;
using RZ.UnitTest.Wrapper;
using Xunit;

namespace RZ.UnitTest.Web
{
    public class HtmlLegacyParserTest_GetHtmlCompleteTag
    {
        [Fact]
        public void SimpleCompleteTag()
        {
            var lexer = new HtmlLexerWrapper("<br />");
            var parser = new HtmlLegacyParserWrapper(lexer);

            HtmlContent content;

            parser.GetHtmlCompleteTag(out content).Should().BeTrue();

            var completeTag = (HtmlContentCompleteTag)content;

            completeTag.TagName.ToString().Should().Be("br");
            completeTag.IsClosed.Should().BeTrue();
            completeTag.Attributes.Should().BeEmpty();
        }

        [Fact]
        public void CompleteTagWithOneAttribute()
        {
            var lexer = new HtmlLexerWrapper("<img href=\"http://www.yahoo.com/img.png\" />");
            var parser = new HtmlLegacyParserWrapper(lexer);

            HtmlContent content;

            parser.GetHtmlCompleteTag(out content).Should().BeTrue();

            var completeTag = (HtmlContentCompleteTag)content;

            completeTag.TagName.ToString().Should().Be("img");
            completeTag.IsClosed.Should().BeTrue();
            completeTag.Attributes.Count.Should().Be(1);

            completeTag.Attributes.ContainsKey("href").Should().BeTrue();
            completeTag.Attributes["href"].Should().Be("http://www.yahoo.com/img.png");
        }

        [Fact]
        public void OpenTag()
        {
            var lexer = new HtmlLexerWrapper("<code>Bold Text</code>");
            var parser = new HtmlLegacyParserWrapper(lexer);

            HtmlContent content;
            parser.GetHtmlCompleteTag(out content).Should().BeFalse();

            lexer.Cursor.Should().Be(0);
        }
    }
}
