﻿using System;
using FluentAssertions;
using RZ.UnitTest.Wrapper;
using Xunit;

namespace RZ.UnitTest.Web
{
    public class HtmlParserTest_Lexer
    {
        [Fact]
        public void TestCreationState()
        {
            HtmlLexerWrapper lexer = new HtmlLexerWrapper("<br />");

            lexer.Content.Should().Be("<br />");
            lexer.Cursor.Should().Be(0);
            lexer.EOC.Should().BeFalse();
        }

        [Fact]
        public void GetBeginOpenTag()
        {
            HtmlLexerWrapper lexer = new HtmlLexerWrapper("<br />");

            String tagName;
            lexer.GetBeginOpenTag(out tagName).Should().BeTrue();
            tagName.Should().Be("br");

            lexer.Cursor.Should().Be(3);
            lexer.EOC.Should().BeFalse();
        }

        [Fact]
        public void GetBeginOpenTag_SingleLetter()
        {
            var lexer = new HtmlLexerWrapper("<b>Bold Text</b>");

            String tagName;
            lexer.GetBeginOpenTag(out tagName).Should().BeTrue();

            tagName.Should().Be("b");
            lexer.Cursor.Should().Be(2);
            lexer.EOC.Should().BeFalse();
        }

        [Fact]
        public void GetTagName()
        {
            var lexer = new HtmlLexerWrapper("br oken w ord 123 abc");

            String tagName;

            lexer.GetTagName(out tagName).Should().BeTrue();
            tagName.Should().Be("br");

            lexer.SkipWhiteSpaces();
            lexer.GetTagName(out tagName).Should().BeTrue();
            tagName.Should().Be("oken");

            lexer.SkipWhiteSpaces();
            lexer.GetTagName(out tagName).Should().BeTrue();
            tagName.Should().Be("w");

            lexer.SkipWhiteSpaces();
            lexer.GetTagName(out tagName).Should().BeTrue();
            tagName.Should().Be("ord");

            lexer.SkipWhiteSpaces();
            lexer.GetTagName(out tagName).Should().BeFalse();

            lexer.SkipWhiteSpaces();
            lexer.GetTagName(out tagName).Should().BeFalse();
        }

        [Fact]
        public void GetTagName_SingleLetterWithSymbol()
        {
            var lexer = new HtmlLexerWrapper("A, is my friend!");

            String tagName;
            lexer.GetTagName(out tagName).Should().BeTrue();
            tagName.Should().Be("A");
        }

        [Fact]
        public void SkipCompleteEndOpenTagAtEnd()
        {
            var lexer = new HtmlLexerWrapper("    />");

            lexer.SkipCompleteEndOpenTag();

            lexer.EOC.Should().BeTrue();
        }

        [Fact]
        public void SkipCompleteEndOpenTag_FollowBySomething()
        {
            var lexer = new HtmlLexerWrapper("    />123 as number");

            lexer.SkipCompleteEndOpenTag();

            lexer.EOC.Should().BeFalse();
            lexer.Cursor.Should().Be(6);
        }

        [Fact]
        public void GetQuoteText_WithSingleQuote()
        {
            var lexer = new HtmlLexerWrapper("' Hello/World! '  />");

            String text;
            lexer.GetQuoteText(out text).Should().BeTrue();

            " Hello/World! ".Should().Be(text);
            lexer.Cursor.Should().Be(16);
        }

        [Fact]
        public void GetQuoteText_WithDoubleQuote()
        {
            var lexer = new HtmlLexerWrapper("\" Hello/World! \"  />");

            String text;
            lexer.GetQuoteText(out text).Should().BeTrue();

            " Hello/World! ".Should().Be(text);
            lexer.Cursor.Should().Be(16);
        }

        [Fact]
        public void GetQuoteText_InvalidCase()
        {
            var lexer = new HtmlLexerWrapper("  \" Hello/World! \"  />");

            String text;
            lexer.GetQuoteText(out text).Should().BeFalse();
            lexer.Cursor.Should().Be(0);
        }

        [Fact]
        public void GetNormalText()
        {
            var lexer = new HtmlLexerWrapper("Hello, <b>world</b>!");

            String text;
            lexer.GetNormalText(out text).Should().BeTrue();
            text.Should().Be("Hello, ");
            lexer.EOC.Should().BeFalse();
            lexer.Cursor.Should().Be(7);
        }

        [Fact]
        public void GetSingleCompromisedWord()
        {
            var lexer = new HtmlLexerWrapper("Geeez!> normal content<br>");

            String text;
            lexer.GetSingleCompromisedWord(out text).Should().BeTrue();
            text.Should().Be("Geeez!");
        }

        [Fact]
        public void GetSingleCompromiseWord_NoText()
        {
            var lexer = new HtmlLexerWrapper("> normal content<br>");

            String text;
            lexer.GetSingleCompromisedWord(out text).Should().BeTrue();
            text.Should().BeEmpty();
        }
    }
}
