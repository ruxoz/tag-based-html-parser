﻿using FluentAssertions;
using RZ.Web;
using System;
using Xunit;

namespace RZ.UnitTest.Web
{
    public class HtmlContentBlockTest
    {
        [Fact]
        public void Create_FromCompleteTag()
        {
            var parser = new HtmlParser("<img src=\"abc.jpg\" />Picture");

            parser.MoveToHeadTag().Should().BeTrue();

            var completeTag = (HtmlContentCompleteTag)parser.CurrentContent;

            var block = new HtmlContentBlock(completeTag);

            block.IsClosed.Should().BeTrue();
            block.Count.Should().Be(0);
            block.IsEmpty.Should().BeTrue();
        }

        [Fact]
        public void Function_FindTag_WithOneLevelTags()
        {
            var parser = new HtmlParser(
@"<head>
		<meta http-equiv=""Content-Type"" content=""text/html; charset=UTF-8"" />
<link rel=""shortcut icon"" href=""/favicon.ico"" />
<script type= ""text/javascript"">
			var skin = ""monobook"";
			var stylepath = ""/mywiki/skins"";
        </script> <break>
</head>"
                );

            parser.MoveToHeadTag();

            Boolean hasMatch;
            var block = parser.GrabCurrentTag(out hasMatch);

            hasMatch.Should().BeTrue();

            block.FindTag("meta",   null).Should().BeEquivalentTo(new [] { 0 });
            block.FindTag("link",   null).Should().BeEquivalentTo(new [] { 1 });
            block.FindTag("script", null).Should().BeEquivalentTo(new [] { 2 });
            block.FindTag("break",  null).Should().BeEquivalentTo(new [] { 3 });
        }

        static readonly String MultilevelTagHtml =
@"<html>
<head>
    <title>Test page</title>
    <meta http-equiv=""Content-Type"" content=""text/html; charset=UTF-8"" />
</head>
<body>
<i>Once upon the time</i>, there was a technology named <a href=""http://wikipedia.org/Hyperlink""><b>HyperLink</b></a>.
    <blockquote>
    The technology was very <b>exciting</b>!
    </blockquote>
</body>
</html>";

        [Fact]
        public void Function_FindTag_WithMultipleLevelTags()
        {
            var block = ConvertToBlock(MultilevelTagHtml);

            block.FindTag("head",  null).Should().BeEquivalentTo(new [] { 0    });
            block.FindTag("title", null).Should().BeEquivalentTo(new [] { 0, 0 });
            block.FindTag("meta",  null).Should().BeEquivalentTo(new [] { 0, 1 });
            block.FindTag("body",  null).Should().BeEquivalentTo(new [] { 1    });
            block.FindTag("i",     null).Should().BeEquivalentTo(new [] { 1, 0 });
            block.FindTag("a",     null).Should().BeEquivalentTo(new [] { 1, 2 });
            block.FindTag("b",     null).Should().BeEquivalentTo(new [] { 1, 2,0 });

            block.FindTag("blockquote",null).Should().BeEquivalentTo(new [] { 1, 4 });
        }

        [Fact]
        public void Function_FindTag_WithMultipleLevelTags_Index()
        {
            var block = ConvertToBlock(MultilevelTagHtml);

            block.FindTag("b", new [] { 1, 0 }).Should().BeEquivalentTo(new [] { 1, 2, 0 });
            block.FindTag("b", new [] { 1, 4 }).Should().BeEquivalentTo(new [] { 1, 4, 1 });
            block.FindTag("b", new[] { 1, 2, 0 }).Should().BeEquivalentTo(new [] { 1, 4, 1 });
        }

        [Fact]
        public void Function_FindTag_WithInvalidIndex()
        {
            var block = ConvertToBlock(MultilevelTagHtml);

            Action action = () => block.FindTag("b", new [] { 1, 1 });

            action.ShouldThrow<IndexOutOfRangeException>();
        }

        HtmlContentBlock ConvertToBlock(String html)
        {
            var parser = new HtmlParser(html);

            parser.MoveToHeadTag();

            Boolean hasMatch;
            var block = parser.GrabCurrentTag(out hasMatch);

            return block;
        }
    }
}
