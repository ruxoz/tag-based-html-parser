﻿using System;
using System.ComponentModel;
using FluentAssertions;
using RZ.Web;
using Xunit;

namespace RZ.UnitTest.Web
{
    public class HtmlParserTest
    {
        [Fact]
        public void MoveTo_SimpleCompletedTag_InMiddleContent_WithOneTag()
        {
            var parser = new HtmlParser("hello <br /> world!");

            parser.MoveToTag("br").Should().BeTrue();

            var content = parser.CurrentContent as HtmlContentCompleteTag;

            content.Should().NotBeNull();
            content.TagName.ToString().Should().Be("br");
            content.IsClosed.Should().BeTrue();
            content.Attributes.Should().BeEmpty();
        }

        [Fact]
        public void MovTo_OpenTag_InMiddleContent_WithOneTag()
        {
            var parser = new HtmlParser("hello <b>bold</b> text.");

            parser.MoveToTag("b").Should().BeTrue();

            var content = parser.CurrentContent;

            content.Should().BeOfType<HtmlContentOpenTag>();
        }

        [Fact]
        public void MoveToSecondOpenTag()
        {
            var parser = new HtmlParser("hello <b>bold</b> and <i>italic</i> text.");

            parser.MoveToTag("i").Should().BeTrue();

            var content = (HtmlContentOpenTag)parser.CurrentContent;

            content.TagName.ToString().Should().Be("i");
            content.Attributes.Should().BeEmpty();
            content.IsClosed.Should().BeFalse();
        }

        [Fact]
        public void MoveToInvalidOpenTag()
        {
            var parser = new HtmlParser("hello <b>bold</b> text");

            parser.MoveToTag("i").Should().BeFalse();

            parser.EndOfContent.Should().BeTrue();
        }

        [Fact]
        [Category("Grab Tag")]
        public void GrabTag()
        {
            var parser = new HtmlParser("hello <b>bold</b> text");

            parser.MoveToTag("b").Should().BeTrue();

            Boolean hasMatch;
            var block = parser.GrabCurrentTag(out hasMatch);

            hasMatch.Should().BeTrue();
            block.TagName.ToString().Should().Be("b");
            block.ToString().Should().Be("<b>bold</b>");
            block.Count.Should().Be(1);
            block[0].ToString().Should().Be("bold");

            var currentContent = (HtmlContentText)parser.FetchNextContent();

            currentContent.ToString().Should().Be(" text");
        }

        [Fact]
        [Category("Grab Tag")]
        public void GrabTagWithInnerTag()
        {
            var parser = new HtmlParser("This is <b>bold, <i>bold italic</i></b> style.");

            parser.MoveToTag("b").Should().BeTrue();

            Boolean hasMatch;
            var block = parser.GrabCurrentTag(out hasMatch);

            hasMatch.Should().BeTrue();
            block.TagName.ToString().Should().Be("b");
            block.ToString().Should().Be("<b>bold, <i>bold italic</i></b>");
            block.Count.Should().Be(2);
            block[0].Should().BeOfType<HtmlContentText>();
            block[1].Should().BeOfType<HtmlContentBlock>();
            block[0].ToString().Should().Be("bold, ");
            block[1].ToString().Should().Be("<i>bold italic</i>");

            var innerBlock = (HtmlContentBlock)block[1];
            innerBlock.Count.Should().Be(1);
            innerBlock[0].ToString().Should().Be("bold italic");

            parser.CurrentContent.Should().BeOfType<HtmlContentCloseTag>();
        }

        const String ComplexHtmlWithScript =
@"Html is
<form method=""post"">Good<input type=""radio"" name=""good"">
<div style=""color: red"">or Bad <input type=""radio"" name=""bad"" > huh?</div>
</form>
<script language=""javascript"">
var x = ""<form >"";
//</script>
alert(x);</script>";

        const String ComplexHtmlWithColonName = "<html xmlns:vml=\"urn:schemas-microsoft-com:vml\" ><head></head><body></body></html>";

        [Fact]
        [Category("Grab Tag")]
        public void GrabComplexHtml_Form()
        {
            var parser = new HtmlParser(ComplexHtmlWithScript);

            parser.MoveToTag("form").Should().BeTrue();

            Boolean hasMatch;
            var block = parser.GrabCurrentTag(out hasMatch);

            hasMatch.Should().BeTrue();
            block.TagName.ToString().Should().Be("form");
            block.IsClosed.Should().BeTrue();

            var closeTag = (HtmlContentCloseTag)parser.CurrentContent;

            closeTag.TagName.ToString().Should().Be("form");
        }

        [Fact]
        public void GrabComplexHtml_SecondTag()
        {
            var parser = new HtmlParser(ComplexHtmlWithScript);

            parser.MoveToTag("form").Should().BeTrue();

            parser.MoveToTag("input").Should().BeTrue();

            Boolean hasMatch;
            var block = parser.GrabCurrentTag(out hasMatch);

            hasMatch.Should().BeFalse();
            block.TagName.ToString().Should().Be("input");
            block.Attributes.Count.Should().Be(2);
            block.IsClosed.Should().BeTrue();
            block.Attributes.ContainsKey("type").Should().BeTrue();
            block.Attributes.ContainsKey("name").Should().BeTrue();
            block.Attributes["type"].Should().Be("radio");
            block.Attributes["name"].Should().Be("good");
        }

        [Fact]
        public void GrabComplexHtml_ThirdTag()
        {
            var parser = new HtmlParser(ComplexHtmlWithScript);

            parser.MoveToTag("form").Should().BeTrue();

            parser.MoveToTag("div").Should().BeTrue();

            Boolean hasMatch;
            var block = parser.GrabCurrentTag(out hasMatch);

            hasMatch.Should().BeTrue();
            block.TagName.ToString().Should().Be("div");
            block.Attributes.Count.Should().Be(1);
            block.IsClosed.Should().BeTrue();
            block.Attributes.ContainsKey("style").Should().BeTrue();
            block.Attributes["style"].ToString().Should().Be("color: red");
            block.Count.Should().Be(3);
        }

        [Fact]
        public void GrabComplexHtml_SecondForm()
        {
            var parser = new HtmlParser(ComplexHtmlWithScript);

            parser.MoveToTag("form").Should().BeTrue();
            parser.MoveToTag("form").Should().BeFalse();
        }

        [Fact]
        public void GrabComplexHtml_JScript()
        {
            var parser = new HtmlParser(ComplexHtmlWithScript);

            parser.MoveToTag("script").Should().BeTrue();

            Boolean hasMatch;
            var content = parser.GrabCurrentTag(out hasMatch);

            hasMatch.Should().BeTrue();
            content.TagName.ToString().Should().Be("script");
            content.Count.Should().Be(1);
            content.IsClosed.Should().BeTrue();

            String expected = @"
var x = ""<form >"";
//</script>
alert(x);";
            content[0].ToString().Should().Be(expected);
        }

        [Fact]
        public void ComplexHtmlWithColonName_Parse()
        {
            var parser = new HtmlParser(ComplexHtmlWithColonName);

            parser.MoveToTag("html").Should().BeTrue();

            var content = (HtmlContentOpenTag)parser.CurrentContent;

            content.TagName.ToString().Should().Be("html");
            content.Attributes.Count.Should().Be(1);
            content.Attributes.ContainsKey("xmlns:vml").Should().BeTrue();
            content.Attributes["xmlns:vml"].ToString().Should().Be("urn:schemas-microsoft-com:vml");
        }

        [Fact]
        public void MovePass_TagNameWithSymbol()
        {
            var parser = new HtmlParser(
@"<head>
		<meta http-equiv=""Content-Type"" content=""text/html; charset=UTF-8"" />
<link rel=""shortcut icon"" href=""/favicon.ico"" />"
            );

            parser.MoveToTag("link").Should().BeTrue();

            var openTag = (HtmlContentCompleteTag)parser.CurrentContent;

            openTag.Attributes.Count.Should().Be(2);
            openTag.Attributes["rel"].Should().Be("shortcut icon");
        }

        [Fact]
        public void MovePass_ScriptTag_WithSpaceInAttribute()
        {
            var parser = new HtmlParser(
@"<script type= ""text/javascript"">
			var skin = ""monobook"";
			var stylepath = ""/mywiki/skins"";
        </script> <break>"
                );

            parser.MoveToTag("break").Should().BeTrue();
        }

        [Fact]
        public void MovePass_ScriptTag_WithVersionAndCase()
        {
            var parser = new HtmlParser(
@"<script language= ""JAVASCRIPT1.2"">
			var skin = ""monobook"";
			var stylepath = ""/mywiki/skins"";
        </script> <break>"
                );
            parser.MoveToTag("break").Should().BeTrue();
        }

        [Fact]
        public void MovePass_ProblemTag()
        {
            var parser = new HtmlParser("<b>normal tag</b> <i>and <code>problem </codeW</i> <u>tag</u>");

            Action action = () => parser.MoveToTag("u");

            action.ShouldThrow<HtmlParserException>();
        }

        [Fact]
        public void MoveToFirstTag()
        {
            var parser = new HtmlParser("  Hello <b>world!</b> ");

            parser.MoveToHeadTag().Should().BeTrue();

            var headTag = (HtmlContentHeadTag)parser.CurrentContent;

            headTag.TagName.ToString().Should().Be("b");
        }
    }
}
