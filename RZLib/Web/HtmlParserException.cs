﻿//
// Author: Ruxo Zheng (http://ruxozheng.spaces.live.com/)
//
// This file is distributed under CPOL 1.0 License (http://www.codeproject.com/info/cpol10.aspx).
//

using System;

namespace RZ.Web
{
    public sealed class HtmlParserException : Exception
    {
        public Int32 Position { get; private set; }
        public String UnknownContent { get; private set; }

        const Int32 TextSampleLength = 64;

        public HtmlParserException(String message, Int32 position, String unknownContent)
            : base(message)
        {
            this.UnknownContent = unknownContent.Substring(position, Math.Min(TextSampleLength, unknownContent.Length-position));
            this.Position = position;
        }

        public override string Message
        {
            get { return base.Message + ", at " + Position + ":\r\n" + UnknownContent; }
        }
    }
}
