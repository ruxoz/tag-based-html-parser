﻿//
// Author: Ruxo Zheng (http://ruxozheng.spaces.live.com/)
//
// This file is distributed under CPOL 1.0 License (http://www.codeproject.com/info/cpol10.aspx).
//

using System;
using System.Collections.Generic;

namespace RZ.Web
{
    sealed class HtmlTagStack
    {
        public const Int32 NPos = -1;

        List<HtmlTagName> stack = new List<HtmlTagName>();
        Int32 stackPointer = NPos;

        public Int32 Count
        {
            get { return this.stackPointer + 1; }
        }

        public void Push(HtmlTagName tagName)
        {
            ++this.stackPointer;
            if (this.stackPointer < this.stack.Count)
                this.stack[this.stackPointer] = tagName;
            else
                this.stack.Add(tagName);
        }

        public void RemoveFrom(Int32 pointer)
        {
            if (pointer > this.stackPointer)
                return;

            CodeValidator.ArgumentValidIf(pointer >= 0);

            this.stackPointer = pointer -1;
        }

        public Int32 LastIndexOf(HtmlTagName tagName)
        {
            Int32 pointer = stackPointer;

            while (pointer >= 0)
            {
                if (this.stack[pointer] == tagName)
                    break;

                --pointer;
            }
            return pointer;
        }
    }
}
