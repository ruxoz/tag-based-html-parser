﻿//
// Author: Ruxo Zheng (http://ruxozheng.spaces.live.com/)
//
// This file is distributed under CPOL 1.0 License (http://www.codeproject.com/info/cpol10.aspx).
//

using System.Collections.Generic;

namespace RZ.Web
{
    public interface IHtmlPartialParser{
        HtmlContent GetNextContent();
    }
    public interface IHtmlParser : IHtmlPartialParser, IEnumerable<HtmlContent>{}
}
