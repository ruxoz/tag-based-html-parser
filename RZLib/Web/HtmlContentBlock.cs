﻿//
// Author: Ruxo Zheng (http://ruxozheng.spaces.live.com/)
//
// This file is distributed under CPOL 1.0 License (http://www.codeproject.com/info/cpol10.aspx).
//

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RZ.Collections.Extensions;

namespace RZ.Web
{
    /// <summary>
    /// Content block contains only itself and HtmlContentText.
    /// </summary>
    public class HtmlContentBlock : HtmlContentHeadTag, IEnumerable<HtmlContent>
    {
        readonly List<HtmlContent> content = new List<HtmlContent>();
        String fullContent;
        String openTag;
        String closeTag;

        public HtmlContentBlock(HtmlContentHeadTag headTag)
            : base(String.Empty, headTag.TagName, headTag.Attributes ?? new HtmlAttributeCollection() , headTag.IsClosed)
        {
            CodeValidator.ThrowArgumentExceptionIf(headTag is HtmlContentBlock);

            openTag = headTag.ToString();

            if (IsClosed)
                closeTag = String.Empty;
        }

        public Int32 Count
        {
            get { return content.Count; }
        }

        public HtmlContent this[int index]
        {
            get { return content[index]; }
        }

        public Boolean IsEmpty
        {
            get { return Count == 0; }
        }

        public void Add(HtmlContentText text)
        {
            ThrowIfAlreadyClosed();
            content.Add(text);

            ResetRepresentText();
        }

        public void Add(HtmlContentBlock block)
        {
            ThrowIfAlreadyClosed();
            content.Add(block);

            ResetRepresentText();
        }

        public void Close(HtmlContentCloseTag tag)
        {
            CodeValidator.ArgumentValidIf(tag.TagName == TagName, tag.TagName + " != " + TagName);
            IsClosed = true;

            closeTag = tag.ToString();
            ResetRepresentText();
        }

        /// <summary>
        /// Use when we cannot find the match close tag.
        /// </summary>
        public void ForceClose()
        {
            IsClosed = true;

            closeTag = "</" + TagName + ">";
            ResetRepresentText();
        }

        public Int32 IndexOfTag(String tagName, Int32 startPosition)
        {
            return content.FindIndex(
                startPosition,
                item => {
                    var c = item as HtmlContentBlock;
                    return c != null && c.TagName == tagName;
                }
                );
        }

        #region Find Methods
        struct BlockInfo
        {
            public HtmlContentBlock Block;
            public Int32 Index;

            public HtmlContentBlock InnerBlock
            {
                get { return (HtmlContentBlock) Block[Index]; }
            }

            public BlockInfo(Int32 index, HtmlContentBlock block)
            {
                Block = block;
                Index = index;
            }
        }

        List<BlockInfo> BuildFindStack(IList<Int32> startPosition)
        {
            var searchStack = new List<BlockInfo>(64);

            if (startPosition != null && startPosition.Count > 0)
            {
                var parentBlock = this;

                foreach (Int32 blockIndex in startPosition)
                {
                    var innerBlock = parentBlock[blockIndex] as HtmlContentBlock;

                    if (innerBlock == null)
                        throw new IndexOutOfRangeException("startPosition does not point to HTML content block.");

                    searchStack.Add(new BlockInfo(blockIndex, parentBlock));

                    parentBlock = innerBlock;
                }
            }

            return searchStack;
        }

        Boolean FindTag(String tagName, List<BlockInfo> searchStack)
        {
            var block = searchStack.Count == 0? this : searchStack.Last().InnerBlock;
            var contentIndex = 0;

            while (true)
            {
                while (contentIndex < block.Count)
                {
                    var innerBlock = block[contentIndex] as HtmlContentBlock;

                    if (innerBlock == null)
                    {
                        ++contentIndex;
                        continue;
                    }

                    searchStack.Push(new BlockInfo(contentIndex, block));

                    if (innerBlock.TagName == tagName)
                        return true;

                    block = innerBlock;
                    contentIndex = 0;
                }

                if (searchStack.Count == 0)
                    return false;

                var lastBlock = searchStack.Pop();

                block = lastBlock.Block;
                contentIndex = lastBlock.Index +1;
            }
        }

        /// <summary>
        /// Find tag name from the specified start position.
        /// </summary>
        /// <exception cref="IndexOutOfRangeException">startPosition is invalid.</exception>
        public IList<Int32> FindTag(String tagName, IList<Int32> startPosition)
        {
            var searchStack = BuildFindStack(startPosition);

            if (FindTag(tagName, searchStack))
            {
                var position = from blockInfo in searchStack select blockInfo.Index;

                return position.ToList();
            }
            else
                return null;
        }
        #endregion

        /// <summary>
        /// Currently use on unmatched block.
        /// </summary>
        /// <param name="anotherBlock">Target block to receive content.</param>
        internal void MoveInnerBlockTo(HtmlContentBlock anotherBlock)
        {
            anotherBlock.content.AddRange(content);
            content.Clear();

            ResetRepresentText();
        }

        #region IEnumerable<HtmlContent> Members

        public IEnumerator<HtmlContent> GetEnumerator()
        {
            return content.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        public override string ToString()
        {
            return fullContent ?? GenerateFullContent();
        }

        void ResetRepresentText()
        {
            fullContent = null;
        }

        string GenerateFullContent()
        {
            var allText = new StringBuilder(openTag.Length + closeTag.Length + 1024);

            allText.Append(openTag);

            foreach (var innerContent in content)
                allText.Append(innerContent);

            allText.Append(closeTag);

            return fullContent = allText.ToString();
        }

        void ThrowIfAlreadyClosed()
        {
            if (IsClosed)
                throw new InvalidOperationException("Block is already closed!");
        }
    }
}
