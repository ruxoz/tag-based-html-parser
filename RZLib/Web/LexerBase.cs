﻿//
// Author: Ruxo Zheng (http://ruxozheng.spaces.live.com/)
//
// This file is distributed under CPOL 1.0 License (http://www.codeproject.com/info/cpol10.aspx).
//

using System;

namespace RZ.Web {
    public abstract class LexerBase {
        protected readonly String content;
        protected Int32 cursor;

        protected LexerBase(String content) {
            this.content = content;
        }
        public String Content {
            get { return content; }
        }
        public Int32 Cursor {
            get { return cursor; }
            set { cursor = value; }
        }
        public Boolean EOC {
            get { return cursor >= content.Length; }
        }

        #region Position save helper
        public Int32 BeginCheck() {
            return cursor;
        }
        public Boolean CommitCheck(bool commit, Int32 savedCursor) {
            if (!commit)
                cursor = savedCursor;
            return commit;
        }
        #endregion

        public String GetContent(Int32 begin) {
            return content.Substring(begin, cursor - begin);
        }

        protected Char L(Int32 offset) {
            return content[cursor + offset];
        }
        protected Int32 ValidContentLength {
            get { return content.Length - cursor; }
        }
        protected Char Fetch() {
            return content[cursor++];
        }
        protected Boolean MoveCursor(bool condition, Int32 step) {
            if (condition)
                cursor = Math.Min(content.Length, cursor + step);
            return condition;
        }
        protected Boolean CheckInsensitive(String text){
            return ValidLength(text.Length) && content.IndexOf(text, cursor, text.Length, StringComparison.InvariantCultureIgnoreCase) != -1;
        }
        protected Boolean ValidLength(Int32 requiredLength) {
            return !EOC && ValidContentLength >= requiredLength;
        }
    }
}
