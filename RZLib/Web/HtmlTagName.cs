﻿//
// Author: Ruxo Zheng (http://ruxozheng.spaces.live.com/)
//
// This file is distributed under CPOL 1.0 License (http://www.codeproject.com/info/cpol10.aspx).
//

using System;

namespace RZ.Web
{
    public struct HtmlTagName
    {
        String tagName;

        public HtmlTagName(String tagName)
        {
            CodeValidator.ArgumentValidIf(tagName != null);
            this.tagName = tagName;
        }

        public static Boolean operator ==(HtmlTagName lhs, String rhs)
        {
            return (String.Compare(lhs.tagName, rhs, true) == 0);
        }

        public static Boolean operator !=(HtmlTagName lhs, String rhs)
        {
            return (String.Compare(lhs.tagName, rhs, true) != 0);
        }

        public static implicit operator String(HtmlTagName name)
        {
            return name.tagName;
        }

        public override bool Equals(object obj)
        {
            HtmlTagName? rhs = obj as HtmlTagName?;
            if (rhs.HasValue)
                return (this == rhs);
            else
                return false;
        }

        public override int GetHashCode()
        {
            return this.tagName.GetHashCode();
        }

        public override string ToString()
        {
            return this.tagName;
        }
    }
}
