﻿//
// Author: Ruxo Zheng (http://ruxozheng.spaces.live.com/)
//
// This file is distributed under CPOL 1.0 License (http://www.codeproject.com/info/cpol10.aspx).
//

using System;
using System.Collections.Generic;

namespace RZ.Web
{
    public class HtmlAttributeCollection : Dictionary<String, String>
    {
        public new String this[String key]
        {
            get { return ContainsKey(key)? base[key.ToLower()] : String.Empty; }
        }

        public new void Add(String key, String Value)
        {
            base.Add(key.ToLower(), Value);
        }

        public new Boolean ContainsKey(String key)
        {
            return base.ContainsKey(key.ToLower());
        }
    }
}
