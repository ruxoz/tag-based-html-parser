﻿//
// Author: Ruxo Zheng (http://ruxozheng.spaces.live.com/)
//
// This file is distributed under CPOL 1.0 License (http://www.codeproject.com/info/cpol10.aspx).
//

using System;

namespace RZ.Web
{
    public abstract class HtmlContent
    {
        protected String OriginalText { get; private set; }

        protected HtmlContent(String originalText)
        {
            this.OriginalText = originalText;
        }

        public override string ToString()
        {
            return this.OriginalText;
        }
    }

    public class HtmlContentText : HtmlContent
    {
        public HtmlContentText(String originalText) : base(originalText) {}
    }

    public class HtmlCommentTag : HtmlContent{
        public HtmlCommentTag(string text) : base("<!--" + text + "-->"){
            InnerText = text;
        }
        public string InnerText { get; private set; }
    }
    public abstract class HtmlContentTag : HtmlContent
    {
        public HtmlTagName TagName { get; private set; }
        public Boolean IsClosed { get; internal protected set; }

        protected HtmlContentTag(String originalText, String tagName, Boolean isCloseTag)
            : base(originalText)
        {
            this.TagName = new HtmlTagName(tagName);
            this.IsClosed = isCloseTag;
        }

        public Boolean IsTagName(String tagName)
        {
            return (String.Compare(tagName, this.TagName, true) == 0);
        }
    }

    public abstract class HtmlContentHeadTag : HtmlContentTag
    {
        public HtmlAttributeCollection Attributes { get; private set; }

        protected HtmlContentHeadTag(String originalText, String tagName, HtmlAttributeCollection attributes, Boolean isCloseTag)
            : base(originalText, tagName, isCloseTag)
        {
            this.Attributes = attributes;
        }
    }

    public class HtmlContentCompleteTag : HtmlContentHeadTag
    {
        public HtmlContentCompleteTag(String originalText, String tagName, HtmlAttributeCollection attributes)
            : base(originalText, tagName, attributes, true)
        {
        }
    }

    public class HtmlContentOpenTag : HtmlContentHeadTag
    {
        public HtmlContentOpenTag(String originalText, String tagName, HtmlAttributeCollection attributes)
            : base(originalText, tagName, attributes, false)
        {
        }
    }

    public class HtmlContentCloseTag : HtmlContentTag
    {
        public HtmlContentCloseTag(String originalText, String tagName)
            : base(originalText, tagName, true)
        {
        }
    }
}
