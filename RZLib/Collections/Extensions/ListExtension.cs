﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RZ.Collections.Extensions
{
    public static class ListExtension
    {
        public static Int32 FindIndex<T>(this IList<T> list, Int32 startPosition, Func<T, bool> predicate)
        {
            var listCount = list.Count;
            for (var searchIndex = startPosition; searchIndex < listCount; ++searchIndex)
            {
                if (predicate(list[searchIndex]))
                    return searchIndex;
            }
            return -1;
        }
        static public void Push<T>(this IList<T> list, T item)
        {
            list.Add(item);
        }
        static public T Pop<T>(this IList<T> list)
        {
            if (list.Count == 0)
                throw new InvalidOperationException("List is empty");
            var result = list.Last();
            list.RemoveAt(list.Count-1);
            return result;
        }
        static public bool IsEmpty<T>(this IList<T> list)
        {
            return list.Count == 0;
        }
    }
}
