﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace RZ
{
    public static class CodeValidator
    {
        public static void ArgumentValidIf(bool condition)
        {
            Debug.Assert(condition, "Argument is invalid!");
            if (!condition)
                throw new ArgumentException();
        }

        public static void ArgumentValidIf(bool condition, String message)
        {
            Debug.Assert(condition, "Argument is invalid!", message);
            if (!condition)
                throw new ArgumentException(message);
        }

        public static void SupportOnlyIf(bool condition)
        {
            Debug.Assert(condition, "Not supported condition!");
            if (!condition)
                throw new NotSupportedException();
        }

        public static void ThrowInvalidOperationIf(bool conditon)
        {
            Debug.Assert(!conditon, "Invalid Operation exception");
            if (conditon)
                throw new InvalidOperationException();
        }

        public static void ThrowArgumentExceptionIf(bool condition)
        {
            Debug.Assert(!condition, "Argument exception");
            if (condition)
                throw new ArgumentException();
        }

        public static void ThrowNotSupportedExceptionIf(bool condition)
        {
            Debug.Assert(!condition, "Not supported exception");
            if (condition)
                throw new NotSupportedException();
        }
    }
}
