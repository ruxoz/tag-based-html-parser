﻿namespace NativeWebSurf
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnParse = new System.Windows.Forms.Button();
            this.inpParam = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cboMethod = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnGo = new System.Windows.Forms.Button();
            this.inpAddress = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.txtResponse = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.scrTagTree = new System.Windows.Forms.TreeView();
            this.scrProperties = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.txtContent = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnResetFind = new System.Windows.Forms.Button();
            this.btnFindTag = new System.Windows.Forms.Button();
            this.inpFindTag = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnParse);
            this.panel1.Controls.Add(this.inpParam);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.cboMethod);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.btnGo);
            this.panel1.Controls.Add(this.inpAddress);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(630, 59);
            this.panel1.TabIndex = 0;
            // 
            // btnParse
            // 
            this.btnParse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnParse.Location = new System.Drawing.Point(543, 30);
            this.btnParse.Name = "btnParse";
            this.btnParse.Size = new System.Drawing.Size(75, 23);
            this.btnParse.TabIndex = 7;
            this.btnParse.Text = "&Parse";
            this.btnParse.UseVisualStyleBackColor = true;
            this.btnParse.Click += new System.EventHandler(this.ButtonOnParse);
            // 
            // inpParam
            // 
            this.inpParam.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.inpParam.Location = new System.Drawing.Point(225, 31);
            this.inpParam.Name = "inpParam";
            this.inpParam.Size = new System.Drawing.Size(312, 20);
            this.inpParam.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(179, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Param:";
            // 
            // cboMethod
            // 
            this.cboMethod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboMethod.FormattingEnabled = true;
            this.cboMethod.Items.AddRange(new object[] {
            "GET",
            "POST"});
            this.cboMethod.Location = new System.Drawing.Point(66, 31);
            this.cboMethod.Name = "cboMethod";
            this.cboMethod.Size = new System.Drawing.Size(91, 21);
            this.cboMethod.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Method";
            // 
            // btnGo
            // 
            this.btnGo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGo.Location = new System.Drawing.Point(543, 4);
            this.btnGo.Name = "btnGo";
            this.btnGo.Size = new System.Drawing.Size(75, 23);
            this.btnGo.TabIndex = 6;
            this.btnGo.Text = "&Go";
            this.btnGo.UseVisualStyleBackColor = true;
            this.btnGo.Click += new System.EventHandler(this.ButtonGoClick);
            // 
            // inpAddress
            // 
            this.inpAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.inpAddress.Location = new System.Drawing.Point(66, 6);
            this.inpAddress.Name = "inpAddress";
            this.inpAddress.Size = new System.Drawing.Size(471, 20);
            this.inpAddress.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Address:";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 59);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(630, 514);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.txtResponse);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(622, 488);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Response";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // txtResponse
            // 
            this.txtResponse.BackColor = System.Drawing.SystemColors.Info;
            this.txtResponse.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtResponse.Location = new System.Drawing.Point(3, 3);
            this.txtResponse.MaxLength = 1000000;
            this.txtResponse.Multiline = true;
            this.txtResponse.Name = "txtResponse";
            this.txtResponse.ReadOnly = true;
            this.txtResponse.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtResponse.Size = new System.Drawing.Size(616, 482);
            this.txtResponse.TabIndex = 0;
            this.txtResponse.WordWrap = false;
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(622, 488);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Request";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.splitContainer1);
            this.tabPage3.Controls.Add(this.panel2);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(622, 488);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Structure";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.scrTagTree);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.scrProperties);
            this.splitContainer1.Panel2.Controls.Add(this.txtContent);
            this.splitContainer1.Size = new System.Drawing.Size(622, 462);
            this.splitContainer1.SplitterDistance = 290;
            this.splitContainer1.TabIndex = 1;
            // 
            // scrTagTree
            // 
            this.scrTagTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scrTagTree.HideSelection = false;
            this.scrTagTree.Location = new System.Drawing.Point(0, 0);
            this.scrTagTree.Name = "scrTagTree";
            this.scrTagTree.Size = new System.Drawing.Size(290, 462);
            this.scrTagTree.TabIndex = 0;
            this.scrTagTree.TabStop = false;
            this.scrTagTree.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.TagTreeOnNodeSelection);
            // 
            // scrProperties
            // 
            this.scrProperties.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.scrProperties.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scrProperties.GridLines = true;
            this.scrProperties.Location = new System.Drawing.Point(0, 0);
            this.scrProperties.Name = "scrProperties";
            this.scrProperties.Size = new System.Drawing.Size(328, 297);
            this.scrProperties.TabIndex = 0;
            this.scrProperties.TabStop = false;
            this.scrProperties.UseCompatibleStateImageBehavior = false;
            this.scrProperties.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Name";
            this.columnHeader1.Width = 113;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Value";
            this.columnHeader2.Width = 178;
            // 
            // txtContent
            // 
            this.txtContent.BackColor = System.Drawing.SystemColors.Info;
            this.txtContent.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txtContent.Location = new System.Drawing.Point(0, 297);
            this.txtContent.Multiline = true;
            this.txtContent.Name = "txtContent";
            this.txtContent.ReadOnly = true;
            this.txtContent.Size = new System.Drawing.Size(328, 165);
            this.txtContent.TabIndex = 1;
            this.txtContent.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnResetFind);
            this.panel2.Controls.Add(this.btnFindTag);
            this.panel2.Controls.Add(this.inpFindTag);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 462);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(622, 26);
            this.panel2.TabIndex = 2;
            // 
            // btnResetFind
            // 
            this.btnResetFind.Location = new System.Drawing.Point(186, 1);
            this.btnResetFind.Name = "btnResetFind";
            this.btnResetFind.Size = new System.Drawing.Size(75, 23);
            this.btnResetFind.TabIndex = 2;
            this.btnResetFind.Text = "&Reset Find";
            this.btnResetFind.UseVisualStyleBackColor = true;
            this.btnResetFind.Click += new System.EventHandler(this.ButtonOnResetFind);
            // 
            // btnFindTag
            // 
            this.btnFindTag.Location = new System.Drawing.Point(109, 1);
            this.btnFindTag.Name = "btnFindTag";
            this.btnFindTag.Size = new System.Drawing.Size(75, 23);
            this.btnFindTag.TabIndex = 1;
            this.btnFindTag.Text = "&Find Tag";
            this.btnFindTag.UseVisualStyleBackColor = true;
            this.btnFindTag.Click += new System.EventHandler(this.FindTagButton_Click);
            // 
            // inpFindTag
            // 
            this.inpFindTag.Location = new System.Drawing.Point(3, 3);
            this.inpFindTag.Name = "inpFindTag";
            this.inpFindTag.Size = new System.Drawing.Size(100, 20);
            this.inpFindTag.TabIndex = 0;
            this.inpFindTag.Enter += new System.EventHandler(this.TagInputOnEnter);
            // 
            // MainWindow
            // 
            this.AcceptButton = this.btnGo;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(630, 573);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel1);
            this.Name = "MainWindow";
            this.Text = "Native Web Surf";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            this.splitContainer1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnGo;
        private System.Windows.Forms.TextBox inpAddress;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox txtResponse;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ComboBox cboMethod;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox inpParam;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnParse;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TreeView scrTagTree;
        private System.Windows.Forms.ListView scrProperties;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.TextBox txtContent;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnFindTag;
        private System.Windows.Forms.TextBox inpFindTag;
        private System.Windows.Forms.Button btnResetFind;
    }
}

